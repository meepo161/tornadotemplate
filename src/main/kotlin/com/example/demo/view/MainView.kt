package com.example.demo.view

import com.example.demo.constants.*
import com.example.demo.controllers.MainController
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import javafx.event.EventHandler
import javafx.geometry.Pos
import javafx.scene.control.TabPane
import javafx.scene.text.FontWeight
import kfoenix.jfxbutton
import kfoenix.jfxcheckbox
import kfoenix.jfxtextfield
import tornadofx.*

class MainView : View(PROJECTNAME) {
    private val authorizationView: AuthorizationView by inject()
    private val mainController: MainController = MainController()

    override val root = borderpane {
        setPrefSize(1366.0, 768.0)
        top {
            menubar {
                menu(PROTOCOL) {
                    item(NEW) {
                        graphic = FontAwesomeIconView(FontAwesomeIcon.FILE_ALT).apply {
                            glyphSize = 16.0
                        }
                    }
                    item(OPEN) {
                        graphic = FontAwesomeIconView(FontAwesomeIcon.FOLDER_OPEN_ALT).apply {
                            glyphSize = 16.0
                        }
                    }
                    item(OPEN_FROM_DB) {
                        graphic = FontAwesomeIconView(FontAwesomeIcon.DATABASE).apply {
                            glyphSize = 16.0
                        }
                    }
                    item(SAVE_AS) {
                        graphic = FontAwesomeIconView(FontAwesomeIcon.SAVE).apply {
                            glyphSize = 16.0
                        }
                    }
                    separator()
                    item(EXIT) {
                        graphic = FontAwesomeIconView(FontAwesomeIcon.HAND_SPOCK_ALT).apply {
                            glyphSize = 16.0
                        }
                        onAction = EventHandler {
                            replaceWith<AuthorizationView>(
                                ViewTransition.Metro(1.0.seconds, ViewTransition.Direction.RIGHT),
                                sizeToScene = true,
                                centerOnScreen = true
                            )
                        }
                    }

                }
                menu(DATABASE) {
                    item(TESTITEMS) {
                        graphic = FontAwesomeIconView(FontAwesomeIcon.SPACE_SHUTTLE).apply {
                            glyphSize = 16.0
                        }
                    }
                    item(PROTOCOLS) {
                        graphic = FontAwesomeIconView(FontAwesomeIcon.LIST_ALT).apply {
                            glyphSize = 16.0
                        }
                    }
                    item(PROFILIES) {
                        graphic = FontAwesomeIconView(FontAwesomeIcon.GROUP).apply {
                            glyphSize = 16.0
                        }
                    }
                    separator()
                    item(IMPORT) {
                        graphic = FontAwesomeIconView(FontAwesomeIcon.CLOUD_DOWNLOAD).apply {
                            glyphSize = 16.0
                        }
                    }
                    item(EXPORT) {
                        graphic = FontAwesomeIconView(FontAwesomeIcon.CLOUD_UPLOAD).apply {
                            glyphSize = 16.0
                        }
                    }

                }
                menu(INSTRUMENTS) {
                    item(DEVICE_STATE) {
                        graphic = FontAwesomeIconView(FontAwesomeIcon.CIRCLE).apply {
                            glyphSize = 16.0
                        }
                    }
                    item(CURRENT_STATE) {
                        graphic = FontAwesomeIconView(FontAwesomeIcon.CIRCLE_ALT).apply {
                            glyphSize = 16.0
                        }
                    }
                    separator()
                    item(ABOUT) {
                        graphic = FontAwesomeIconView(FontAwesomeIcon.INFO_CIRCLE).apply {
                            glyphSize = 16.0
                        }
                        onAction = EventHandler {
                            mainController.viewAboutUs()
                        }
                    }
                    item(BLACK_THEME) {
                        graphic = FontAwesomeIconView(FontAwesomeIcon.BLACK_TIE).apply {
                            glyphSize = 16.0
                        }
                    }
                }
            }
        }
        center {

            tabpane {
                tabClosingPolicy = TabPane.TabClosingPolicy.UNAVAILABLE

                tab(INITIAL_DATA) {
                    anchorpane {

                        vbox(spacing = 200) {
                            alignment = Pos.CENTER
                            anchorpaneConstraints {
                                topAnchor = 0
                                bottomAnchor = 0
                                leftAnchor = 0
                                rightAnchor = 0
                            }

                            hbox(spacing = 360) {
                                alignment = Pos.CENTER

                                vbox(spacing = 24) {
                                    alignment = Pos.CENTER
                                    hbox(spacing = 16) {
                                        alignment = Pos.CENTER
                                        label(FILL_FIELDS) {
                                            style {
                                                fontWeight = FontWeight.BOLD
                                            }
                                        }
                                    }
                                    hbox(spacing = 24) {
                                        alignment = Pos.CENTER
                                        label(FACTORY_NUMBER) {

                                        }
                                        jfxtextfield {
                                            minWidth = 320.0
                                        }
                                    }
                                    hbox(spacing = 16) {
                                        alignment = Pos.CENTER
                                        label(TESTITEM) {

                                        }
                                        jfxtextfield {
                                            minWidth = 320.0
                                        }
                                    }

                                }
                                vbox(spacing = 24) {
                                    alignment = Pos.CENTER
                                    hbox(spacing = 16) {
                                        alignment = Pos.CENTER
                                        label(SELECT_EXPERIMENTS) {
                                            style {
                                                fontWeight = FontWeight.BOLD
                                            }
                                        }
                                    }
                                    hbox(spacing = 16) {
                                        jfxcheckbox(EXPERIMENT1) {

                                        }
                                    }
                                    hbox(spacing = 16) {
                                        jfxcheckbox(EXPERIMENT2) {

                                        }
                                    }
                                    hbox(spacing = 16) {
                                        jfxcheckbox(EXPERIMENT3) {

                                        }
                                    }

                                }
                            }
                            hbox(spacing = 16) {
                                alignment = Pos.CENTER

                                jfxbutton(CLEAR) {

                                }
                                jfxbutton(START_EXPERIMENTS) {

                                }
                            }
                        }
                    }
                }
            }
        }
    }
}