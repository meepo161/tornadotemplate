package com.example.demo.view

import com.example.demo.constants.AUTHORIZATION
import com.example.demo.constants.AUTHORIZATIONBUTTON
import com.example.demo.constants.LOGIN
import com.example.demo.constants.PASSWORD
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView
import javafx.event.EventHandler
import javafx.geometry.Pos
import javafx.scene.text.FontWeight
import kfoenix.jfxbutton
import kfoenix.jfxpasswordfield
import kfoenix.jfxtextfield
import tornadofx.*

class AuthorizationView : View(AUTHORIZATION) {
    override val root = anchorpane {
        setPrefSize(1024.0, 720.0)

        vbox(spacing = 24) {
            alignment = Pos.CENTER
            anchorpaneConstraints {
                topAnchor = 0
                bottomAnchor = 0
                leftAnchor = 0
                rightAnchor = 0
            }
            hbox(spacing = 16) {
                alignment = Pos.CENTER
                label(AUTHORIZATION) {
                    style {
                        fontSize = 28.px
                        fontWeight = FontWeight.BOLD
                    }
                }

            }
            hbox(spacing = 27) {
                alignment = Pos.CENTER
                label(LOGIN) {
                    graphic = FontAwesomeIconView(FontAwesomeIcon.MALE)

                }
                jfxtextfield {

                }
            }
            hbox(spacing = 16) {
                alignment = Pos.CENTER
                label(PASSWORD) {
                    graphic = FontAwesomeIconView(FontAwesomeIcon.KEY)
                }
                jfxpasswordfield {

                }
            }
            hbox {
                alignment = Pos.CENTER
                jfxbutton(AUTHORIZATIONBUTTON) {
                    isDefaultButton = true
                    graphic = FontAwesomeIconView(FontAwesomeIcon.SIGN_IN)

                    onAction = EventHandler {
                        replaceWith<MainView>(
                                ViewTransition.FadeThrough(1.0.seconds),
                                sizeToScene = true,
                                centerOnScreen = true
                        )
                    }
                }/*.style = "-fx-background-color: #000000;"*/
            }
        }
    }
}