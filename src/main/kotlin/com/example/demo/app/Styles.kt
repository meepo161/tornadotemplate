package com.example.demo.app

import kfoenix.JFXStylesheet.Companion.jfxCheckBox
import kfoenix.JFXStylesheet.Companion.jfxCheckedColor
import kfoenix.JFXStylesheet.Companion.jfxFocusColor
import tornadofx.Stylesheet
import tornadofx.c

class Styles : Stylesheet() {
    companion object {
    }

    init {
        root {
            accentColor = c("#0096C9")

            button {
                backgroundColor += c("#CCC")
            }

            jfxCheckBox {
                jfxCheckedColor.value = c("#0096C9")
            }

            textField {
                jfxFocusColor.value = c("#0096C9")
            }
        }
    }
}