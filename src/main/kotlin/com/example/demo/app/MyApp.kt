package com.example.demo.app

import com.example.demo.view.AuthorizationView
import javafx.stage.Stage
import tornadofx.*

class MyApp : App(AuthorizationView::class, Styles::class) {
    init {
        reloadViewsOnFocus()
    }

    override fun start(stage: Stage) {
        with(stage) {
            minWidth = 1360.0
            minHeight = 720.0
//            maxWidth = 640.0
//            maxHeight = 480.0
            super.start(this)
        }
    }
}