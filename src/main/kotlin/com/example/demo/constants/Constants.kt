package com.example.demo.constants

val AUTHORIZATION = "Авторизация"
val LOGIN = "Логин"
val PASSWORD = "Пароль"
val AUTHORIZATIONBUTTON = "Авторизоваться"
val PROJECTNAME = "ИМЯ ПРОЕКТА"
val PROTOCOL = "Протокол"
val DATABASE = "База данных"
val INSTRUMENTS = "Инструменты"
val NEW = "Новый"
val OPEN = "Открыть"
val OPEN_FROM_DB = "Открыть из базы данных"
val SAVE_AS = "Сохранить как"
val EXIT = "Выход"
val TESTITEMS = "Объекты испытания"
val PROTOCOLS = "Протоколы"
val PROFILIES = "Профили"
val IMPORT = "Импорт..."
val EXPORT = "Экспорт..."
val DEVICE_STATE = "Состояние устройств"
val CURRENT_STATE = "Состояние защит"
val ABOUT = "О нас"
val BLACK_THEME = "Темная тема"
val INITIAL_DATA = "Исходные данные"
val CLEAR = "Очистить"
val START_EXPERIMENTS = "Начать испытания"
val FILL_FIELDS = "Заполните поля"
val FACTORY_NUMBER = "Заводской номер"
val TESTITEM = "Объект испытания"
val SELECT_EXPERIMENTS = "Выберите испытания"
val EXPERIMENT1 = "Испытание холостого хода"
val EXPERIMENT2 = "Испытание короткого замыкания"
val EXPERIMENT3 = "Испытание №3"


object Info {
    val TITLE = "ШАБЛОН"
    val VERSION = "ООО НПП АВЭМ, Новочеркасск\n" +
            "Разработчик: Иванов И.И.\n" +
            "+7988-888-88-88"
    val DATE = "Версия: 1.0.0\n" +
            "Дата: 01.01.2001"
}