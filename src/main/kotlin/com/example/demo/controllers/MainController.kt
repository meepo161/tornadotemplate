package com.example.demo.controllers

import com.example.demo.constants.Info
import javafx.scene.control.Alert

class MainController {
    fun viewAboutUs() {
        val alert = Alert(Alert.AlertType.INFORMATION)
        alert.title = Info.TITLE
        alert.headerText = Info.VERSION
        alert.contentText = Info.DATE
        alert.showAndWait()
    }
}